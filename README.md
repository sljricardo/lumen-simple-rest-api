# LUMEN base REST API

You can use this as a base example to start build a REST API with CRUD operations.

This projects use a sqlite DB with simple fields, you can play with all operations GET, POST, PUT, DELETE 

You can even extend functionality with some authentication (JWL) if you like.