<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

// GET ALL articles
$router->get('/', function(){   
    return DB::table('articles')->get();
});

// GET Especific article
$router->get('/{articleId}', function($articleId){    
    
    $article = DB::table('articles')->where("id", "=", $articleId);

    if( $article->count() )
    {
        return $article->get();
    }

    return "Article not found";
});

// POST Information
$router->post('/add', function(Request $request){

    // Validate the post fields
    $fields = $this->validate($request, [
        "name"        => "required",
        "description" => "required",
        "author"      => "required"
    ]);

    DB::table('articles')->insert($fields);

    return "Success";
});

// PUT Update info from specific article
$router->put('/{articleId}', function(Request $request, $articleId){

    DB::table('articles')->where("id", "=", $articleId)->update($request->all());

    return "Success";
});

// DELETE a specific article
$router->delete('/{articleId}', function($articleId){

    DB::table('articles')->where("id", "=", $articleId)->delete();

    return "Success";
});